﻿using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInput : MonoBehaviour
    {
        public PlayerController PlayerController;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            bool jumpPressed = Input.GetButtonDown("Jump");
            bool jumpReleased = Input.GetButtonUp("Jump");
            bool fire1Pressed = Input.GetButtonDown("Fire1");
            float horizontalMovementDirection = Input.GetAxisRaw("Horizontal");
            PlayerController.ReadInputs(jumpPressed, jumpReleased, fire1Pressed, horizontalMovementDirection);
        }

        private void FixedUpdate()
        {

        }
    }
}

