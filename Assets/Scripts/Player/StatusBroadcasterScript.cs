﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Player
{
    public class StatusBroadcasterScript : MonoBehaviour
    {

        public PlayerController PlayerController;
        public PlayerInput PlayerInput;
        public Text TextObject; 

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            string isGrounded = PlayerController.IsGrounded ? "Is grounded!" : "Not grounded";

            TextObject.text = isGrounded; 
        }
    }
}


