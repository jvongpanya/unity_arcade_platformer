﻿using Assets.Scripts.Combat.Interface;

namespace Assets.Scripts.Player.Weapons.Normal
{
    public class NormalWeapon : WeaponBase
    {
        public NormalWeapon()
        {
            Attack = new NormalAttack(); 
        }
    }
}
