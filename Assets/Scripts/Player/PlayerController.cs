﻿using Assets.Scripts.Combat;
using Assets.Scripts.Combat.Interface;
using Assets.Scripts.Player.Weapons.Normal;
using Assets.Scripts.Shared.Enums;
using Assets.Scripts.Shared.Interface;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Player
{
    public class PlayerController : PhysicalObject, IHasHitpoints
    {

        #region Attack items
        public float Attack1RecoveryTime;
        public Hitbox hitBox;
        public override Hitbox Hitbox { get { return hitBox; } protected set { value = hitBox; } }
        public override WeaponBase Weapon { get; protected set; }
        #endregion

        #region DoInput Items 
        private float _horizontalMovementDirection;
        private bool _jumpPressed;
        private bool _jumpReleased;
        private float _attack1RemainingRecovery;
        private bool _fire1Pressed; 
        #endregion

        // Start is called before the first frame update
        void Start()
        {
            HitPoints = 200;
            Weapon = new NormalWeapon(); 
        }

        protected override void Update_Internal()
        {
            if(_attack1RemainingRecovery <= 0)
            {
                if (_fire1Pressed)
                {
                    Attack();
                    _fire1Pressed = false;
                    _attack1RemainingRecovery = Attack1RecoveryTime;
                }

                // Handle jump pressed here, since we need to jump the instant jump occurs. 
                if (_jumpPressed && IsGrounded)
                {
                    CurrentRigidbody2d.AddForce(new Vector2(0f, JumpSpeed));
                    _jumpPressed = false;
                }

            }
        }

        protected override void FixedUpdate_Internal()
        {
            bool isAttacking = _attack1RemainingRecovery > 0;
            Move( _jumpReleased, _horizontalMovementDirection * Time.deltaTime, isAttacking);
            if (_jumpReleased) _jumpReleased = false;
        }

        public void ReadInputs(bool jumpPressed, bool jumpReleased, bool fire1Pressed, float horizontalMovementDirection)
        {
            if (_attack1RemainingRecovery <= 0)
            {
                _horizontalMovementDirection = horizontalMovementDirection;
                _jumpPressed = jumpPressed;
                _jumpReleased = jumpReleased;
                _fire1Pressed = fire1Pressed; 
            }
            else
            {
                _attack1RemainingRecovery -= Time.deltaTime;
                // Disable all potential input
                _jumpPressed = false;
                _horizontalMovementDirection = 0;
            }

        }


        public override void Attack()
        {
            CurrentAnimator.SetTrigger(AnimationVariables.attack1Pressed.ToString());
            hitBox.Attack = Weapon.Attack; 
        }

        public void Move(bool jumpReleased, float horizontalMoveDirectionAndSpeed, bool attacking)
        {
            if ((horizontalMoveDirectionAndSpeed > 0 && !IsFacingRight) || (horizontalMoveDirectionAndSpeed < 0 && IsFacingRight))
            {
                FlipCharacter();
            }

            float finalMoveSpeed = horizontalMoveDirectionAndSpeed * MoveSpeed * 10f;

            

            CurrentRigidbody2d.velocity = new Vector2(finalMoveSpeed, 
                attacking // Stop Y velocity if attacking 
                || (jumpReleased  && CurrentRigidbody2d.velocity.y >= 0) // Or, stop Y velocity if jump is released and is not already falling 
                ? 0 : CurrentRigidbody2d.velocity.y);


            CurrentAnimator.SetBool(AnimationVariables.isAirborne.ToString(), !IsGrounded);
            CurrentAnimator.SetBool(AnimationVariables.isRunning.ToString(), horizontalMoveDirectionAndSpeed != 0 && IsGrounded);
            
        }

        public override void WhenDestroyed()
        {
            // nothing yet
        }

        
    }
}

