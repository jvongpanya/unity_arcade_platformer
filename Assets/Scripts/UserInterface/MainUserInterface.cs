﻿using Assets.Scripts.Player;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.UserInterface
{
    public class MainUserInterface : MonoBehaviour
    {
        public Text HitPointsText;
        public Text ScoreText;
        public Text ResetText; 
        public PlayerController PlayerController; 
        public int CurrentScore { get; set; }

        private void Start()
        {
            CurrentScore = 0;
            ResetText.enabled = false; 
        }

        private void Update()
        {
            if(PlayerController.HitPoints > 0)
            {
                UpdateScoreWhileAlive();
            }
            else
            {
                ShowFinalScore();
                CheckForReset();
            }
            
        }

        private void UpdateScoreWhileAlive()
        {
            HitPointsText.text = $"Hit Points: {PlayerController.HitPoints}";
            ScoreText.text = $"Current Score: {CurrentScore}";
        }

        private void ShowFinalScore()
        {
            HitPointsText.enabled = false;
            ScoreText.enabled = false;
            ResetText.enabled = true;
            ResetText.text = $"Final Score: {CurrentScore}";
            ResetText.text += Environment.NewLine + "Press the \"R\" key to reset!";
        }

        private void CheckForReset()
        {
            if (PlayerController.HitPoints <= 0 && Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
            }
        }
    }
}
