﻿using Assets.Scripts.Player;
using Assets.Scripts.Shared.Enums;
using Assets.Scripts.Shared.Interface;
using UnityEngine;
using Assets.Scripts.Combat.Interface;
using Assets.Scripts.Combat;
using Assets.Scripts.UserInterface;
using System.Collections;
using Assets.Scripts.Enemies.Weapons.Normal;

namespace Assets.Scripts.Enemies
{
    public class Enemy1 : PhysicalObject
    {
        #region ICanAttack
        public Hitbox hitBox; 
        public override Hitbox Hitbox { get { return hitBox; } protected set { value = hitBox; } }
        public override WeaponBase Weapon { get; protected set; }
        #endregion 

        private MainUserInterface _mainUserInterface; 

        public int Score { get; private set; }

        private bool _underPlatform;
        private bool _inJumpZone;
        private bool _onPlatform;
        private float _playerPlaneThreshold;
        private float _currentMovementDirection;

        private void Start()
        {
            HitPoints = 300;
            Score = 100; 
            Weapon = new NormalEnemyWeapon();
            _mainUserInterface = GameObject.FindObjectOfType<MainUserInterface>();
            _underPlatform = false;
            _playerPlaneThreshold = 3.2f;
            _currentMovementDirection = 1; 
        }

        private void Move()
        {
            PlayerController playerObject = FindObjectOfType<PlayerController>();
            if (playerObject.HitPoints > 0)
            {
                Vector3 playerLocation = playerObject.transform.position;
                float xMoveDirection = playerLocation.x - transform.position.x > 0 ? 1 : -1; // If negative, it's front. Otherwise, it's behind. 
                bool playerAbove = playerObject.transform.position.y > transform.position.y && playerObject.transform.position.y - transform.position.y > _playerPlaneThreshold;
                bool playerBelow = playerObject.transform.position.y < transform.position.y && transform.position.y - playerObject.transform.position.y > _playerPlaneThreshold;
                float finalMoveSpeed;

                if((playerAbove && _underPlatform) || (playerBelow && _onPlatform))
                {
                    finalMoveSpeed = MoveSpeed * 10f * _currentMovementDirection; 
                }
                else
                {
                    StartCoroutine(WaitToTurn(xMoveDirection)); 
                    finalMoveSpeed = MoveSpeed * 10f * _currentMovementDirection;

                    if (((xMoveDirection > 0 && !IsFacingRight) || (xMoveDirection < 0 && IsFacingRight)))
                    {
                        FlipCharacter();
                    }

                }
                
                CurrentRigidbody2d.velocity = new Vector2(finalMoveSpeed, CurrentRigidbody2d.velocity.y);
                
                if (!_underPlatform && playerAbove && IsGrounded && _inJumpZone)
                {
                    // if not under platform and player is above, jump
                    CurrentRigidbody2d.AddForce(new Vector2(0f, JumpSpeed));
                }
                CurrentAnimator.SetBool(AnimationVariables.isRunning.ToString(), xMoveDirection != 0 && IsGrounded);
                CurrentAnimator.SetBool(AnimationVariables.isAirborne.ToString(), !IsGrounded);
            }
            else
            {
                CurrentRigidbody2d.velocity = new Vector2(0, transform.position.y);
                CurrentAnimator.SetBool(AnimationVariables.isRunning.ToString(), false);
                CurrentAnimator.SetBool(AnimationVariables.isAirborne.ToString(), !IsGrounded);
            }
            
        }

        protected override void Update_Internal()
        {
            // Do nothing yet 
            Attack(); 
        }

        protected override void FixedUpdate_Internal()
        {
            DetermineSpaceAndCollisions(); 
            Move();
        }

        private void DetermineSpaceAndCollisions()
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 1);
            bool underPlatform = false;
            bool inJumpZone = false;
            bool onPlatform = false;
            foreach (Collider2D collider in colliders)
            {
                if (collider.tag.Equals("underPlatform"))
                    underPlatform = true;

                if (collider.tag.Equals("jumpZone"))
                    inJumpZone = true;

                if (collider.tag.Equals("platform"))
                    onPlatform = true;
            }
            _underPlatform = underPlatform;
            _inJumpZone = inJumpZone;
            _onPlatform = onPlatform;
        }

        public override void Attack()
        {
            Hitbox.Attack = Weapon.Attack; 
        }

        public override void WhenDestroyed()
        {
            if(_mainUserInterface != null)
            {
                _mainUserInterface.CurrentScore += Score; 
            }
            StartCoroutine("DestroySelfAfter3Seconds");
            
        }

        private IEnumerator DestroySelfAfter3Seconds()
        {
            yield return new WaitForSeconds(3);
            Destroy(gameObject);
        }

        private IEnumerator WaitToTurn(float newMoveDirection)
        {
            yield return new WaitForSeconds(0.5f);
            _currentMovementDirection = newMoveDirection; 
        }
    }
}
