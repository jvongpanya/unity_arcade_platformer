﻿using Assets.Scripts.Combat.Interface;

namespace Assets.Scripts.Enemies.Weapons.Normal
{
    public class NormalEnemyWeapon : WeaponBase
    {
        public NormalEnemyWeapon()
        {
            Attack = new NormalEnemyAttack(); 
        }
    }
}
