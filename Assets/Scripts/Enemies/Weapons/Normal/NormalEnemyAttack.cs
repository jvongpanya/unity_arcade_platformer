﻿using Assets.Scripts.Combat.Interface;

namespace Assets.Scripts.Enemies.Weapons.Normal
{
    public class NormalEnemyAttack : AttackBase
    {
        public NormalEnemyAttack()
        {
            HitStun = .5f;
            Pushback = 250f; 
        }
    }
}
