﻿using Assets.Scripts.Player;
using System;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
    public class EnemySpawner: MonoBehaviour
    {
        public GameObject EnemyToSpawn;              
        public float SpawnTime = 5f;          
        public Transform[] SpawnPoints;        
        public PlayerController Player; 


        void Start()
        {
            // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
            InvokeRepeating("Spawn", SpawnTime, SpawnTime);
        }


        void Spawn()
        {
            // If the player has no health left...
            if (Player.HitPoints > 0)
            {
                int spawnPointIndex = UnityEngine.Random.Range(0, SpawnPoints.Length);
                
                Instantiate(EnemyToSpawn, SpawnPoints[spawnPointIndex].position, SpawnPoints[spawnPointIndex].rotation);
            }
        }
    }
}
