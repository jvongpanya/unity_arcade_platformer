﻿using Assets.Scripts.Combat;
using Assets.Scripts.Combat.Interface;
using Assets.Scripts.Shared.Enums;
using UnityEngine;

namespace Assets.Scripts.Shared.Interface
{
    public abstract class PhysicalObject : MonoBehaviour, IHasHitpoints, ICanAttack
    {
        #region Required Physical Parts
        public Rigidbody2D CurrentRigidbody2d;
        #endregion

        #region Ground check status 
        public LayerMask GroundCheckLayerMask;
        public Transform GroundCheckTransform;
        public float GroundCheckRadius;
        public bool IsGrounded { get; protected set; }
        #endregion

        #region HitPoints
        public int HitPoints { get; set; }
        public float HitStunTime { get; set; }
        public float InvulnerableTime { get; set; }
        public bool IsInvulnerable { get
            {
                return HitStunTime > 0;
            }
        }

        #endregion

        #region ICanAttack
        public abstract WeaponBase Weapon { get; protected set; }
        public abstract void Attack();
        public abstract Hitbox Hitbox { get; protected set; }
        #endregion 
        

        void Update()
        {
            if(HitStunTime > 0)
            {
                HitStunTime -= Time.deltaTime;
                CurrentAnimator.SetFloat(AnimationVariables.hitStunTime.ToString(), HitStunTime);
            }
            else
            {
                CurrentAnimator.SetFloat(AnimationVariables.hitStunTime.ToString(), -1);
                Update_Internal();
            }

            if (InvulnerableTime > 0)
            {
                InvulnerableTime -= Time.deltaTime;
                CurrentAnimator.SetFloat(AnimationVariables.invulnerableTime.ToString(), InvulnerableTime);
            }
            else
            {
                CurrentAnimator.SetFloat(AnimationVariables.invulnerableTime.ToString(), -1);
            }

        }


        protected abstract void Update_Internal();

        void FixedUpdate()
        {

            Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheckTransform.position, GroundCheckRadius, GroundCheckLayerMask);
            IsGrounded = colliders.Length > 0;

            // If alive and not in hit stun
            if (HitStunTime <= 0 && HitPoints > 0)
            {
                FixedUpdate_Internal();
            }
        }

        protected abstract void FixedUpdate_Internal(); 

        public virtual void OnDamage(AttackBase attack, PhysicalObject attacker)
        {
            
            if (!this.IsInvulnerable && this.HitPoints > 0)
            {
                this.HitPoints -= attack.Damage;
                if (this.HitPoints <= 0)
                {
                    CurrentAnimator.SetTrigger(AnimationVariables.isDestroyed.ToString());
                    gameObject.layer = 13; // Dead body layer
                    foreach(Transform child in gameObject.GetComponentsInChildren<Transform>())
                    {
                        // set child layers
                        child.gameObject.layer = 13; 
                    }
                    CurrentRigidbody2d.constraints = RigidbodyConstraints2D.FreezePositionX;
                    Hitbox.gameObject.SetActive(false); // Disable hitbox
                    WhenDestroyed(); 
                }
                else
                {
                    // This way, only one state gets triggered. 
                    this.HitStunTime = HitStunTime < 0 ? attack.HitStun : attack.HitStun + HitStunTime;
                    this.InvulnerableTime = attack.HitStun - 0.5f;
                }
            }
        }

        public abstract void WhenDestroyed(); 

        public void DestroyBehavior()
        {
            Destroy(gameObject);
        }


        #region Animation
        public Animator CurrentAnimator;
        #endregion
        
        #region Physics items
        public float MoveSpeed;
        public float JumpSpeed;
        public bool IsFacingRight = true;
        #endregion


        protected void FlipCharacter()
        {
            IsFacingRight = !IsFacingRight;
            Vector3 newScale = transform.localScale;
            newScale.x *= -1;
            transform.localScale = newScale;
        }
    }
}
