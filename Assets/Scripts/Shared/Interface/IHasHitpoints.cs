﻿using Assets.Scripts.Combat.Interface;

namespace Assets.Scripts.Shared.Interface
{
    public interface IHasHitpoints
    {
        int HitPoints { get; set; }
        float HitStunTime { get; set; }
        float InvulnerableTime { get; set; }
        bool IsInvulnerable { get; }
        void OnDamage(AttackBase attack, PhysicalObject defender);
        void DestroyBehavior(); 
    }
}
