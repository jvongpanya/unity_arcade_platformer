﻿namespace Assets.Scripts.Shared.Enums
{
    public enum AnimationVariables
    {
        isRunning,
        isAirborne,
        hitStunTime,
        isDestroyed,
        invulnerableTime,
        attack1Pressed
    }
}
