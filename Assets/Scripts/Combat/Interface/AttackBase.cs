﻿using Assets.Scripts.Shared.Interface;
using UnityEngine;

namespace Assets.Scripts.Combat.Interface
{

    public abstract class AttackBase
    {
        public virtual int Damage { get; protected set; }

        public virtual float HitStun { get; protected set; }

        public virtual float Pushback { get; protected set; }

        public AttackBase()
        {
            Damage = 100;
            HitStun = 1;
            Pushback = 200f; 
        }

        public void DoAttack(PhysicalObject attacker, PhysicalObject defender)
        {
            DoPushback(attacker, defender);
            defender.OnDamage(this, attacker);
        }

        private void DoPushback(PhysicalObject attacker, PhysicalObject defender)
        {
            float attackDirection = attacker.transform.position.x > defender.transform.position.x ? -1 :
                attacker.transform.position.x < defender.transform.position.x ? 1 : -1;
            Vector2 resetForce = new Vector2(0, 0);
            Vector2 hitForce = new Vector2(attackDirection * Pushback, defender.CurrentRigidbody2d.velocity.y);
            defender.CurrentRigidbody2d.velocity = resetForce; // Make sure there are no other forces acting upon this object
            defender.CurrentRigidbody2d.AddForce(hitForce, ForceMode2D.Force);
        }
    }
}
