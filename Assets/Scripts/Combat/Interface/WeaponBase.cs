﻿namespace Assets.Scripts.Combat.Interface
{
    public abstract class WeaponBase
    {
        public AttackBase Attack { get; protected set; }
    }
}
