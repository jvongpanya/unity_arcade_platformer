﻿using Assets.Scripts.Combat.Interface;
using Assets.Scripts.Shared.Interface;
using UnityEngine;

namespace Assets.Scripts.Combat
{
    public class Hitbox : MonoBehaviour
    {
        public Transform AttackHitboxTransform;
        public float AttackXSize;
        public float AttackYSize;
        public LayerMask EnemyLayerMask;
        public AttackBase Attack { get; set; }
        

        private void Start()
        {

        }

        private void Update()
        {
            if(Attack != null)
            {
                Collider2D[] collidedObjects = Physics2D.OverlapBoxAll(AttackHitboxTransform.position, new Vector2(AttackXSize, AttackYSize), 0, EnemyLayerMask);
                for (int index = 0; index < collidedObjects.Length; index++)
                {
                    Attack.DoAttack(AttackHitboxTransform.parent.gameObject.GetComponent<PhysicalObject>(), collidedObjects[index].gameObject.GetComponent<PhysicalObject>());
                }
            }
        }

        // Hitbox display
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(AttackHitboxTransform.position, new Vector2(AttackXSize, AttackYSize));
        }
    }
}
