﻿using Assets.Scripts.Combat.Interface;namespace Assets.Scripts.Combat
{
    public interface ICanAttack
    {
        WeaponBase Weapon { get; }
        void Attack(); 
        Hitbox Hitbox { get; }
    }
}
